﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebUI.Controllers
{
    public class ParkingController : Controller
    {
        public async Task<IActionResult> Index()
        {
            using var client = new HttpClient();

            var result = await client.GetAsync("https://localhost:44374/api/parking/capacity");
            ViewBag.Capacity = await result.Content.ReadAsStringAsync();

            return View();
        }
    }
}