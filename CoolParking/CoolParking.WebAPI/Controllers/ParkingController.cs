﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public ParkingController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet("balance")]
        public decimal GetBalance()
        {
            return parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public int GetCapacity()
        {
            return parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public int GetFreePlaces()
        {
            return parkingService.GetFreePlaces();
        }
    }
}