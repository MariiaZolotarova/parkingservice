﻿using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    public class TopUpModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}