﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{

    public class ParkingService : IParkingService
    {
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private static ILogService logService;

        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {
            withdrawTimer = _withdrawTimer;
            logTimer = _logTimer;
            logService = _logService;
            withdrawTimer.Elapsed += WithdrawMoney;
            withdrawTimer.Start();
            logTimer.Elapsed += SaveLogs;
            logTimer.Start();
        }

        private static void SaveLogs(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (!Parking.Instance.TransactionCollection.Any())
            {
                logService.Write(string.Empty);
            }

            foreach (var transaction in Parking.Instance.TransactionCollection)
            {
                logService.Write($"{string.Format("{0:G}", transaction.Time)}: {transaction.Sum.ToString("F")} money withdrawn from vehicle with Id='{transaction.VehicleId}'.");
            }

            Parking.Instance.TransactionCollection.Clear();
        }

        private static void WithdrawMoney(Object source, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vehicle in Parking.Instance.VehicleCollection)
            {
                decimal vehicleWithdraw = Settings.VehicleDictionary[vehicle.VehicleType];
                if(vehicle.Balance<=0)
                {
                    vehicleWithdraw = vehicleWithdraw * Settings.FineCoefficient;
                }
                else if (vehicle.Balance <= vehicleWithdraw)
                {
                    vehicleWithdraw = ((vehicleWithdraw - vehicle.Balance) * Settings.FineCoefficient) + vehicle.Balance;
                }

                vehicle.Balance -= vehicleWithdraw;
                Parking.Instance.ParkingBalance += vehicleWithdraw;
                
                Parking.Instance.TransactionCollection.Add(new TransactionInfo(vehicleWithdraw,vehicle.Id));
            }
        }


        public decimal GetBalance()
        {
            return Parking.Instance.ParkingBalance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - GetVehicles().Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Instance.VehicleCollection);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if(Parking.Instance.VehicleCollection.Count>=Settings.ParkingCapacity)
            {
                throw new InvalidOperationException();
            }
            if (Parking.Instance.VehicleCollection.Any(x => x.Id == vehicle.Id))
            {
                throw new ArgumentException();
            }
            Parking.Instance.VehicleCollection.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (Parking.Instance.VehicleCollection.All(x => x.Id != vehicleId))
            {
                throw new ArgumentException();
            }
            Vehicle vehicle = Parking.Instance.VehicleCollection.First(x => x.Id == vehicleId);
            
            if (vehicle.Balance<0)
            {
                throw new InvalidOperationException();
            }
            
            Parking.Instance.VehicleCollection.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0 || Parking.Instance.VehicleCollection.All(x => x.Id != vehicleId))
            {
                throw new ArgumentException();
            }

            Vehicle vehicle = Parking.Instance.VehicleCollection.First(x => x.Id == vehicleId);
            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Instance.TransactionCollection.ToArray();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void Dispose()
        {
            Parking.Instance.VehicleCollection = new List<Vehicle>();
            Parking.Instance.ParkingBalance = Settings.FirstBalance;
            Parking.Instance.TransactionCollection=new List<TransactionInfo>();
        }
    }
}
