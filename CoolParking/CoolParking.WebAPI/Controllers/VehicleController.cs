﻿using System.Collections.ObjectModel;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public VehicleController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet]
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parkingService.GetVehicles();
        }

        [HttpGet("{id}")]
        public IActionResult GetVehicle(string id)
        {
            if (!Vehicle.IsIdValid(id))
            {
                return BadRequest();
            }

            if (!parkingService.GetVehicles().Any(x => x.Id == id))
            {
                return NotFound();
            }

            Vehicle vehicle = parkingService.GetVehicles().First(x => x.Id == id);

            return Ok(vehicle);
        }

        [HttpPost]
        public IActionResult GetVehicles([FromBody] Vehicle vehicle)
        {
            if (vehicle == null || !Vehicle.IsIdValid(vehicle.Id) || vehicle.Balance < 0)
            {
                return BadRequest();
            }

            parkingService.AddVehicle(vehicle);

            return StatusCode(201, vehicle);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            if (!Vehicle.IsIdValid(id))
            {
                return BadRequest();
            }

            if (!parkingService.GetVehicles().Any(x => x.Id == id))
            {
                return NotFound();
            }

            parkingService.RemoveVehicle(id);

            return NoContent();
        }
    }
}