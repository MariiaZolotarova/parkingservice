﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.ParkingConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\MyTransactions.log";
            ITimerService withdrawTimer = new TimerService(Settings.PeriodOfPaid);
            ITimerService logTimer = new TimerService(Settings.LogInterval);
            ILogService logService = new LogService(logFilePath);
            IParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);
            while (true)
            {
                Console.Clear();
                ShowMenu();
                string str = Console.ReadLine();
                switch (str)
                {
                    case "1":
                        Console.WriteLine($"Поточний баланс паркiнгу: {parkingService.GetBalance()}");
                        break;
                    case "2":
                        Console.WriteLine($"Сума зароблених коштiв за поточний перiод: {parkingService.GetLastParkingTransactions().Sum(x => x.Sum)}");
                        break;
                    case "3":
                        Console.WriteLine($"На паркiнгу вiльних мiсць: {parkingService.GetFreePlaces()} з {parkingService.GetCapacity()}");
                        break;
                    case "4":
                        Console.WriteLine("Транзакцiї паркiнгу за поточний перiод:");
                        Console.WriteLine("Дата                             Номер машини                             Сума");
                        foreach (var transaction in parkingService.GetLastParkingTransactions())
                        {
                            Console.WriteLine($"{transaction.Time.ToString("G")}                {transaction.VehicleId}                                 {transaction.Sum}       ");
                        }
                        break;
                    case "5":
                        Console.WriteLine("Iсторiя транзакцiй:");

                        try
                        {
                            var logText = parkingService.ReadFromLog();
                            Console.WriteLine(logText);
                        }
                        catch(InvalidOperationException exception)
                        {
                            Console.WriteLine("Файл не iснує");
                        }

                        break;
                    case "6":
                        Console.WriteLine("Автомобiлi на паркiнгу:");
                        Console.WriteLine("Номер машини              Тип машини           Баланс");
                        foreach (var parkingVehicle in parkingService.GetVehicles())
                        {
                            Console.WriteLine($"{parkingVehicle.Id}          {parkingVehicle.VehicleType}               {parkingVehicle.Balance}");
                        }
                        break;
                    case "7":
                        Console.WriteLine("Введiть номер автомобiля в форматi: ХХ-YYYY-XX (де X - будь-яка лiтера англiйського алфавiту у верхньому регiстрi, а Y - будь-яка цифра, наприклад DV-2345-KJ).");
                        var plateNumber = Console.ReadLine();
                        if (!Regex.IsMatch(plateNumber ?? string.Empty, @"\A[A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[A-Z][A-Z]\Z"))
                        {
                            Console.WriteLine("Введено некоректне значення номеру автомобiля");
                            break;
                        }

                        Console.WriteLine("Оберiть тип автомобiля: [PassengerCar, Truck, Bus, Motorcycle] ");
                        var typeCar = Console.ReadLine();
                        Enum.TryParse(typeof(VehicleType), typeCar, out var objectVehicleType);
                        if (objectVehicleType == null)
                        {
                            Console.WriteLine("Введено некоректне значення типу автомобiля");
                            break;
                        }
                        var vehicleType = (VehicleType)objectVehicleType;

                        Console.WriteLine("Введiть суму коштiв");
                        var moneyCar = Console.ReadLine();
                        var isBalanceParsed = decimal.TryParse(moneyCar, out var balance);
                        if (!isBalanceParsed || balance <= 0)
                        {
                            Console.WriteLine("Введено некоректне значення суми");
                            break;
                        }
                        var vehicle = new Vehicle(plateNumber, vehicleType, balance);

                        try
                        {
                            parkingService.AddVehicle(vehicle);
                        }
                        catch (ArgumentException exception)
                        {
                            Console.WriteLine("Неможливо поставити машину на парковку, перевiрте введенi данi");
                        }
                        catch (InvalidOperationException exception)
                        {
                            Console.WriteLine("Неможливо поставити машину на парковку, парковка заповнена");
                        }
                        break;
                    case "8":
                        Console.WriteLine("Введiть номер автомобiля в форматi: ХХ-YYYY-XX (де X - будь-яка лiтера англiйського алфавiту у верхньому регiстрi, а Y - будь-яка цифра, наприклад DV-2345-KJ).");
                        var deletePlateNumber = Console.ReadLine();

                        try
                        {
                            parkingService.RemoveVehicle(deletePlateNumber);
                        }
                        catch (ArgumentException exception)
                        {
                            Console.WriteLine("Неможливо видалити машину, перевiрте введенi данi");
                        }
                        catch (InvalidOperationException exception)
                        {
                            Console.WriteLine("Недостатньо коштiв, поповнiть рахунок");
                        }
                        break;
                    case "9":
                        Console.WriteLine("Введiть суму поповнення:");
                        var addSum = Console.ReadLine();
                        var isAddSumParsed = decimal.TryParse(addSum, out var addBalance);
                        if (!isAddSumParsed || addBalance <= 0)
                        {
                            Console.WriteLine("Введено некоректне значення суми");
                            break;
                        }
                        Console.WriteLine("Введiть номер автомобiля в форматi: ХХ-YYYY-XX (де X - будь-яка лiтера англiйського алфавiту у верхньому регiстрi, а Y - будь-яка цифра, наприклад DV-2345-KJ).");
                        var vehicleId = Console.ReadLine();

                        try
                        {
                            parkingService.TopUpVehicle(vehicleId, addBalance);
                        }
                        catch (ArgumentException exception)
                        {
                            Console.WriteLine("Неможливо поповнити рахунок, перевiрте введенi данi");
                        }
                        break;

                    default:
                        Console.WriteLine("Введiть коректне значення 1-9.");
                        break;
                }

                Console.WriteLine("Натиcнiть будь-яку кнопку");
                Console.ReadKey();
            }

        }

        private static void ShowMenu()
        {
            Console.WriteLine("Будь ласка, оберiть операцiю");
            Console.WriteLine("1 - Вивести на екран поточний баланс Паркiнгу");
            Console.WriteLine("2 - Вивести на екран суму зароблених коштiв за поточний перiод");
            Console.WriteLine("3 - Вивести на екран кiлькiсть вiльних мiсць на паркуваннi");
            Console.WriteLine("4 - Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод");
            Console.WriteLine("5 - Вивести на екран iсторiю Транзакцiй");
            Console.WriteLine("6 - Вивести на екран список Тр. засобiв , що знаходяться на Паркiнгу.");
            Console.WriteLine("7 - Поставити Транспортний засiб на Паркiнг.");
            Console.WriteLine("8 - Забрати Транспортний засiб з Паркiнгу.");
            Console.WriteLine("9 - Поповнити баланс конкретного Тр. засобу.");
        }
    }
}
