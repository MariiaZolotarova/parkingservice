﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{

    public sealed class Parking
    {
        public decimal ParkingBalance;
        public List<Vehicle> VehicleCollection = new List<Vehicle>();
        public List<TransactionInfo> TransactionCollection = new List<TransactionInfo>();

        private static Parking instance = null;
        private static readonly object padlock = new object();

        private Parking()
        {
            ParkingBalance = Settings.FirstBalance;
        }

        public static Parking Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Parking();
                    }
                    return instance;
                }
            }
        }
    }
}



